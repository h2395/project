package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pojo.polls;
import pojo.questions;

import static utils.DBUtils.fetchConnection;

public class pollsDao {
	// state : private , non static , non transient data members
	private Connection cn;
	private PreparedStatement pst1, pst2, pst3, pst4;
public Scanner sc=new Scanner(System.in);
	public pollsDao() throws ClassNotFoundException, SQLException {

		cn = fetchConnection();
		String sql = "select * from polls where created_by=?";
		pst1 = cn.prepareStatement(sql);
		pst2 = cn.prepareStatement("insert into polls values(default,?,?,?,?)");
		pst3 = cn.prepareStatement("update polls set title=? where id=?");
		pst4 = cn.prepareStatement("delete from polls where id=?");
		System.out.println("emp dao created...");

	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (pst4 != null)
			pst4.close();
		if (cn != null)
			cn.close();
		System.out.println("emp dao cleaned up !");
	}

	public List<polls> getMyPolls(int userId) throws SQLException {

		List<polls> emps = new ArrayList<>();

		pst1.setInt(1, userId);

		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				emps.add(new polls(rst.getInt(1), rst.getString(2), rst.getTimestamp(3), rst.getTimestamp(4),
						rst.getInt(5)));

		}
		return emps;
	}

	public String addNewPoll(polls poll) throws SQLException {

		pst2.setString(1, poll.getTitle());// nm
		pst2.setTimestamp(2, poll.getStart_datetime());// adr
		pst2.setTimestamp(3, poll.getEnd_datetime());// sal
		pst2.setInt(4, poll.getCreated_by());// dept

		int updateCount = pst2.executeUpdate();
		if (updateCount == 1)
			return "poll details added....";

		return "Adding new poll details failed...";
	}
	public void editQuestions() throws SQLException {
		
		//	PreparedStatement pst1;
			PreparedStatement pst2;
			
		  System.out.println("enter the question id ");
		  int qId = sc.nextInt();
		  sc.nextLine();

		  System.out.println("Please enter updated question ");
		  String question = sc.nextLine();
		  
		  System.out.println("Please enter updated question serial no ");
		  int no = sc.nextInt();
		  
		  pst2 = cn.prepareStatement("update Questions set question_text=?, question_serial_no=? where id=?");
		  
		  pst2.setString(1, question);
		  pst2.setInt(2, no);
		  pst2.setInt(3, qId); 
		  
		  pst2.executeUpdate();
		}
	public String updatePollDetails(int pollId, String newTitle) throws SQLException {

		pst3.setString(1, newTitle);

		pst3.setInt(2, pollId);

		int updateCount = pst3.executeUpdate();
		if (updateCount == 1)
			return "Poll details updated....";
		return "Updating poll details failed...";
	}

	public String deletePoll(int id) throws SQLException {

		pst4.setInt(1, id);

		int updateCount = pst4.executeUpdate();
		if (updateCount == 1)
			return "poll details deleted....";
		return "Deleting poll details failed...";
	}
	
 
public String addNewQuestion(questions question) throws SQLException {
		
		pst2.setString(1, question.getQuestion_text());
		pst2.setString(2, question.getOption_A());
		pst2.setString(3, question.getOption_B());
		pst2.setString(4, question.getOption_C());
		pst2.setString(5, question.getOption_D());// nm
		pst2.setInt(6, question.getPoll_id());
		pst2.setInt(7, question.getQuestion_serial_no());
		
		
		int updateCount = pst2.executeUpdate();
		if (updateCount == 1)
			return "question  added....";

		return "Adding new question details failed...";
	}
public ArrayList<polls> listArchivedPolls() throws SQLException {
	ArrayList<polls> archivedPolls = new ArrayList<polls>();
	PreparedStatement pst2 = cn.prepareStatement("select * from polls where end_datetime<?");
	//set IN params
	pst3.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
	
	try(ResultSet rst = pst3.executeQuery())
	{
		while(rst.next()) {
			System.out.println(rst.getString(2));
			archivedPolls.add(new polls(rst.getInt(1), rst.getString(2), rst.getTimestamp(3), rst.getTimestamp(4), rst.getInt(5)));
		}
	}
	
	return archivedPolls;
}
public ArrayList<polls> listLivePolls() throws SQLException {
	ArrayList<polls> livePolls = new ArrayList<polls>();
	PreparedStatement pst1 = cn.prepareStatement("select * from polls where end_datetime>?");
	 
	pst1.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
	
	try(ResultSet rst = pst1.executeQuery())
	{
		while(rst.next()) {
			System.out.println(rst.getString(2));
			livePolls.add(new polls(rst.getInt(1), rst.getString(2), rst.getTimestamp(3), rst.getTimestamp(4), rst.getInt(5)));
		}
	}
	
	return livePolls;
}
}