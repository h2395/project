package dao;

import java.sql.SQLException;
 

import pojo.users;

public interface UserDaoIf {
	void signUpUser(users user) throws SQLException;
	
	public users signInUser(String email,String password) throws SQLException ;
	
}