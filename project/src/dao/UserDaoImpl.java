package dao;

 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static utils.DBUtils.fetchConnection;
 

import pojo.users;

public class UserDaoImpl implements UserDaoIf {
	private Connection cn;
	Scanner sc=new Scanner(System.in);
	private PreparedStatement pst1,pst2,pst3,pst4;

	public UserDaoImpl() throws SQLException {
		cn = fetchConnection();

		pst1 = cn.prepareStatement("select * from users where email=? and password=?");
		pst2 = cn.prepareStatement("insert into users values(default,?,?,?,?,?,?,?)");
		pst3 = cn.prepareStatement("update users set password=? where email=? ");
		pst4 = cn.prepareStatement("update users set mobile=?,address=? where id=?");
	}

	@Override
	public void signUpUser(users user) throws SQLException {
	 
		pst2.setString(1, user.getEmail()); 
		pst2.setString(2, user.getPassword()); 
		pst2.setString(3, user.getMobile()); 
		pst2.setString(4, user.getName()) ;
		pst2.setString(5, user.getGender()); 
		pst2.setString(6, user.getAddress()); 
		pst2.setDate(7, user.getBirth_date());
		 
		pst2.executeUpdate();
	 
	}
	public users signInUser(String email,String password) throws SQLException {
	 
		pst1.setString(1, email);
		pst1.setString(2, password);
		 users user=null;
	 
		try (ResultSet rst = pst1.executeQuery()) {
			if (rst.next())
				  user=new users(rst.getInt(1), rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(6),rst.getString(7),rst.getDate(8));
		}  
		return user; 	 
	}
	public String updateUserDetails(int id) throws SQLException {
		
	 
		System.out.println("Enter Address: ");
		String address = sc.next();
		System.out.println("Enter mobile : ");
		int mobile = sc.nextInt();
		
		pst4.setInt(1, mobile);
 
		
		pst4.setString(2, address);
		 pst4.setInt(3, id);
		
		int updateCount = pst3.executeUpdate();
		if (updateCount == 1)
			return "Poll details updated....";
		return "Updating poll details failed...";
	}
	public void changePassword(String password, String email) throws SQLException {
		pst3.setString(1, password);
		pst3.setString(2, email);
		
		pst3.executeUpdate();
	}
}
