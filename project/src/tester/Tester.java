package tester;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pojo.polls;
import pojo.users;
import dao.UserDaoImpl;
import dao.pollsDao;

public class Tester {

	private static UserDaoImpl userDao;
	private static pollsDao pollDao;
	static Scanner sc = new Scanner(System.in);

	public static void main(String args[]) throws SQLException {

		boolean exit = true;
		users loggedInUser = null;
		while (exit) {
			System.out.print("CourseName-Pg-Dac , course-group:D4 ,Project-D4-40");
			switch (mainMenu()) {
			case 1:
				loggedInUser = signIn();
				aftersignIn(loggedInUser);
				break;
			case 2:
				signUp();
				break;
			case 3:
				System.out.println("3");
				exit = false;
				break;

			}
		}
		
	}

	public static int mainMenu() {

		while (true) {
			int choice;
			System.out.println("\n 1. SignIn");
			System.out.println("2. SignUp");
			System.out.println("3. Exit  ");
			System.out.print("choice : ");
			choice = sc.nextInt();
			if (choice == 3) {
				System.out.println("Thank You !!! you quit");
				System.exit(0);
			}

			else if (choice > 0 && choice <= 2)
				return choice;
			else
				System.out.println("Invalid Choice !!");
		}
	}

	private static void signUp() throws SQLException {
		System.out.println("Enter User Information : ");
		userDao = new UserDaoImpl();
		System.out.print("email : ");
		String email = sc.next();
		System.out.print("password : ");
		String password = sc.next();
		System.out.print("mobile : ");
		String mobile = sc.next();
		System.out.print("name : ");
		String name = sc.next();
		System.out.print("gender : ");
		String gender = sc.next();
		System.out.print("address : ");
		String address = sc.next();
		System.out.print("birth_date : ");
		String birth_d = sc.next();
		Date birth_date = Date.valueOf(birth_d);
		users user = new users(email, password, mobile, name, gender, address, birth_date);

		userDao.signUpUser(user);

	}

	private static void aftersignIn(users user) throws SQLException {
		boolean flag = true;
		while (flag) {
			switch (signInMenu()) {
			case 0:
				break;
			case 1:
				selfPollManagement(user);
				break;
			case 2:
				ArrayList<polls>publicPolls=pollDao.listLivePolls(); 
				publicPolls.forEach(System.out::println);
				break;
			case 3:
				System.out.println("Enter New Password");
				String password=sc.next();
				userDao.changePassword(password,user.getEmail());
				
				break;
			case 4:
				userDao.updateUserDetails(user.getId());
				break;
			case 5:
				break;
			case 6:
				ArrayList<polls>polls=pollDao.listArchivedPolls();
				polls.forEach(System.out::println);
				break;

			default:
				break;
			}
		}
	}

	private static void selfPollManagement(users user) throws SQLException {
		polls poll = null;
		boolean flag = true;
		while (flag) {
			switch (selfPollMenu()) {
			case 0:
				break;
			case 1:
				poll = takePollDetails(user);
				pollDao.addNewPoll(poll);
				break;
			case 2:
				System.out.println("enter poll id");
				int pollid = sc.nextInt();
				System.out.println("enter poll title");
				String polltitle = sc.next();
				pollDao.updatePollDetails(pollid, polltitle);
				break;
			case 3:
				
				break;
			case 5:
				pollDao.editQuestions();
				break;
			case 4:
				System.out.println("enter poll id");
				pollid = sc.nextInt();
				pollDao.deletePoll(pollid);
				break;
			case 9:
				List<polls>mypolls=pollDao.getMyPolls(user.getId());
				mypolls.forEach(x->System.out.println(x.toString()));
				break;

			default:
				break;
			}
		}
	}

	private static users signIn() throws SQLException {
		System.out.println("Login here !!: ");
		userDao = new UserDaoImpl();
		System.out.print("email : ");
		String email = sc.next();
		System.out.print("password : ");
		String password = sc.next();
		return userDao.signInUser(email, password);

	}

	public static int signInMenu() {

		while (true) {
			int choice;
			System.out.println("\n 0. exit");
			System.out.println("\n 1. Self pollManagement");
			System.out.println("2. public polls");
			System.out.println("3. change password  ");
			System.out.println("4. edit profile ");
			System.out.println("5. Sign Out ");
			System.out.println("6. list Archive Polls  ");
			System.out.print("choice : ");
			choice = sc.nextInt();
			if (choice == 0) {
				System.out.println("Thank You !!! you quit");
				System.exit(0);
			}

			else if (choice > 0 && choice <= 6)
				return choice;
			else
				System.out.println("Invalid Choice !!");
		}
	}

	public static polls takePollDetails(users user) {

		System.out.print("enter Polls Details : ");
		System.out.print("title : ");
		String title = sc.next();
		System.out.print(" start_datetime : ");
		String start_dt = sc.next();
		Timestamp start_datetime = Timestamp.valueOf(start_dt);
		System.out.print("end_datetime : ");
		String end_dt = sc.next();
		Timestamp end_datetime = Timestamp.valueOf(end_dt);
		polls poll = new polls(0, title, start_datetime, end_datetime, user.getId());
		return poll;
	}

	public static int selfPollMenu() {

		while (true) {
			int choice;
			System.out.println("\n 0. exit");
			System.out.println("\n 1. Create New Poll");
			System.out.println("2. Edit Poll");
			System.out.println("3. Add Question in Poll  ");
			System.out.println("4. Delete Question from Poll ");
			System.out.println("5. Edit Question from Poll ");
			System.out.println("7. View Poll Result Summary ");
			System.out.println("8. View Poll Result Details");
			System.out.println("9. My Polls");
			System.out.print("choice : ");
			choice = sc.nextInt();
			if (choice == 0) {
				System.out.println("Thank You !!! you quit");
				System.exit(0);
			}

			else if (choice > 0 && choice <= 9)
				return choice;
			else
				System.out.println("Invalid Choice !!");
		}
	}

}
