package pojo;

import java.sql.Timestamp;

public class polls {
	private int id;
	private String title;
	private Timestamp start_datetime;
	private Timestamp end_datetime;
	private int created_by;
	public polls() {
		super();
		// TODO Auto-generated constructor stub
	}
	public polls(int id, String title, Timestamp start_datetime, Timestamp end_datetime, int created_by) {
		super();
		this.id = id;
		this.title = title;
		this.start_datetime = start_datetime;
		this.end_datetime = end_datetime;
		this.created_by = created_by;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Timestamp getStart_datetime() {
		return start_datetime;
	}
	public void setStart_datetime(Timestamp start_datetime) {
		this.start_datetime = start_datetime;
	}
	public Timestamp getEnd_datetime() {
		return end_datetime;
	}
	public void setEnd_datetime(Timestamp end_datetime) {
		this.end_datetime = end_datetime;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	@Override
	public String toString() {
		return "polls [id=" + id + ", title=" + title + ", start_datetime=" + start_datetime + ", end_datetime="
				+ end_datetime + ", created_by=" + created_by + "]";
	}
	
	
}